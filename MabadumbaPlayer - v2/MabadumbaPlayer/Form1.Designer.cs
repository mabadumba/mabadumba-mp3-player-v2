﻿namespace MabadumbaPlayer
{
    partial class Mabadumba
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mabadumba));
            this.button_Play = new System.Windows.Forms.Button();
            this.listBox_Song = new System.Windows.Forms.ListBox();
            this.button_SelectURL = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.textBox_URL = new System.Windows.Forms.TextBox();
            this.button_shuffle = new System.Windows.Forms.Button();
            this.button_OpenURL = new System.Windows.Forms.Button();
            this.button_Loop = new System.Windows.Forms.Button();
            this.label_Status = new System.Windows.Forms.Label();
            this.panel_MediaPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            ((System.ComponentModel.ISupportInitialize)(this.panel_MediaPlayer)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Play
            // 
            this.button_Play.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button_Play.Location = new System.Drawing.Point(8, 94);
            this.button_Play.Margin = new System.Windows.Forms.Padding(4);
            this.button_Play.Name = "button_Play";
            this.button_Play.Size = new System.Drawing.Size(109, 37);
            this.button_Play.TabIndex = 1;
            this.button_Play.Text = "Play";
            this.button_Play.UseVisualStyleBackColor = true;
            this.button_Play.Click += new System.EventHandler(this.button_Play_Click);
            // 
            // listBox_Song
            // 
            this.listBox_Song.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.listBox_Song.FormattingEnabled = true;
            this.listBox_Song.ItemHeight = 20;
            this.listBox_Song.Location = new System.Drawing.Point(617, 13);
            this.listBox_Song.Margin = new System.Windows.Forms.Padding(4);
            this.listBox_Song.Name = "listBox_Song";
            this.listBox_Song.Size = new System.Drawing.Size(392, 444);
            this.listBox_Song.TabIndex = 3;
            this.listBox_Song.SelectedIndexChanged += new System.EventHandler(this.listBox_Song_SelectedIndexChanged);
            this.listBox_Song.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBox_Song_MouseDoubleClick);
            // 
            // button_SelectURL
            // 
            this.button_SelectURL.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button_SelectURL.Location = new System.Drawing.Point(8, 9);
            this.button_SelectURL.Margin = new System.Windows.Forms.Padding(4);
            this.button_SelectURL.Name = "button_SelectURL";
            this.button_SelectURL.Size = new System.Drawing.Size(109, 32);
            this.button_SelectURL.TabIndex = 4;
            this.button_SelectURL.Text = "Select URL";
            this.button_SelectURL.UseVisualStyleBackColor = true;
            this.button_SelectURL.Click += new System.EventHandler(this.button_SelectURL_Click);
            // 
            // textBox_URL
            // 
            this.textBox_URL.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBox_URL.Location = new System.Drawing.Point(132, 12);
            this.textBox_URL.Name = "textBox_URL";
            this.textBox_URL.Size = new System.Drawing.Size(262, 27);
            this.textBox_URL.TabIndex = 5;
            // 
            // button_shuffle
            // 
            this.button_shuffle.BackColor = System.Drawing.Color.Red;
            this.button_shuffle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button_shuffle.Location = new System.Drawing.Point(233, 93);
            this.button_shuffle.Name = "button_shuffle";
            this.button_shuffle.Size = new System.Drawing.Size(86, 38);
            this.button_shuffle.TabIndex = 6;
            this.button_shuffle.Text = "shuffle";
            this.button_shuffle.UseVisualStyleBackColor = false;
            this.button_shuffle.Click += new System.EventHandler(this.button_shuffle_Click);
            // 
            // button_OpenURL
            // 
            this.button_OpenURL.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button_OpenURL.Location = new System.Drawing.Point(8, 50);
            this.button_OpenURL.Name = "button_OpenURL";
            this.button_OpenURL.Size = new System.Drawing.Size(109, 35);
            this.button_OpenURL.TabIndex = 7;
            this.button_OpenURL.Text = "Open URL";
            this.button_OpenURL.UseVisualStyleBackColor = true;
            this.button_OpenURL.Click += new System.EventHandler(this.button_OpenURL_Click);
            // 
            // button_Loop
            // 
            this.button_Loop.BackColor = System.Drawing.Color.Red;
            this.button_Loop.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button_Loop.Location = new System.Drawing.Point(141, 94);
            this.button_Loop.Name = "button_Loop";
            this.button_Loop.Size = new System.Drawing.Size(72, 38);
            this.button_Loop.TabIndex = 8;
            this.button_Loop.Text = "Loop";
            this.button_Loop.UseVisualStyleBackColor = false;
            this.button_Loop.Click += new System.EventHandler(this.button_Loop_Click);
            // 
            // label_Status
            // 
            this.label_Status.AutoSize = true;
            this.label_Status.Location = new System.Drawing.Point(138, 50);
            this.label_Status.Name = "label_Status";
            this.label_Status.Size = new System.Drawing.Size(38, 17);
            this.label_Status.TabIndex = 9;
            this.label_Status.Text = "label";
            // 
            // panel_MediaPlayer
            // 
            this.panel_MediaPlayer.Enabled = true;
            this.panel_MediaPlayer.Location = new System.Drawing.Point(8, 138);
            this.panel_MediaPlayer.Name = "panel_MediaPlayer";
            this.panel_MediaPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("panel_MediaPlayer.OcxState")));
            this.panel_MediaPlayer.Size = new System.Drawing.Size(386, 318);
            this.panel_MediaPlayer.TabIndex = 10;
            this.panel_MediaPlayer.PlayStateChange += new AxWMPLib._WMPOCXEvents_PlayStateChangeEventHandler(this.panel_MediaPlayer_PlayStateChange);
            // 
            // Mabadumba
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 589);
            this.Controls.Add(this.panel_MediaPlayer);
            this.Controls.Add(this.label_Status);
            this.Controls.Add(this.button_Loop);
            this.Controls.Add(this.button_OpenURL);
            this.Controls.Add(this.button_shuffle);
            this.Controls.Add(this.textBox_URL);
            this.Controls.Add(this.button_SelectURL);
            this.Controls.Add(this.listBox_Song);
            this.Controls.Add(this.button_Play);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Mabadumba";
            this.Text = "Mabadumba";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Mabaduma_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.panel_MediaPlayer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button_Play;
        private System.Windows.Forms.ListBox listBox_Song;
        private System.Windows.Forms.Button button_SelectURL;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox textBox_URL;
        private System.Windows.Forms.Button button_shuffle;
        private System.Windows.Forms.Button button_OpenURL;
        private System.Windows.Forms.Button button_Loop;
        private System.Windows.Forms.Label label_Status;
        private AxWMPLib.AxWindowsMediaPlayer panel_MediaPlayer;
    }
}

