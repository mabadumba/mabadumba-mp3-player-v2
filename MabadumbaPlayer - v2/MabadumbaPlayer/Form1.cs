﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace MabadumbaPlayer
{
    public partial class Mabadumba : Form
    {
        bool state_buttonShuffle = false;
        bool state_buttonLoop = false;

        int selected_songIndex;

        string str = "";

        //////////////////////////////
        //////////////////////////////


        public Mabadumba()
        {
            InitializeComponent();

            DirectoryInfo d = new DirectoryInfo("C:\\Users\\KaaN\\Desktop\\müzik");// Going to file path
            textBox_URL.Text = "C:\\Users\\KaaN\\Desktop\\müzik";

            button_Play.Enabled = false;
            button_Loop.Enabled= false;
            button_shuffle.Visible = false;
        }


        //////////////////////////////
        //////////////////////////////
        

        private void button_SelectURL_Click(object sender, EventArgs e)
        { 
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    DirectoryInfo d = new DirectoryInfo(fbd.SelectedPath);// Going to file path

                    textBox_URL.Text = fbd.SelectedPath;

                    FileInfo[] Files = d.GetFiles("*.mp3"); // Getting mp3 files

                    // listbox temizleniyor
                    listBox_Song.Items.Clear();

                    foreach (FileInfo file in Files)
                    {
                        str = file.Name;
                        listBox_Song.Items.Add(str);
                        str = "";
                    }
                }
            }
        }

        private void button_OpenURL_Click(object sender, EventArgs e)
        {
            button_Loop.Enabled = true;

            DirectoryInfo d = new DirectoryInfo(textBox_URL.Text);// Going to file path

            FileInfo[] Files = d.GetFiles("*.mp3"); // Getting mp3 files

            foreach (FileInfo file in Files)
            {
                str = file.Name;
                listBox_Song.Items.Add(str);
                str = "";
            }

            button_Play.Enabled = true;
        }

        private void button_Play_Click(object sender, EventArgs e)
        {
            // karısık calma aktifse rastgele bir sarkı baslatılıyor
            if (state_buttonShuffle)
            {
                play_Shuffle();    
            }
            // herhangi bir sarkı secilmemisse birinci sarkı baslatılıyor
            else if (listBox_Song.SelectedItem == null)
            {
                play_Selection(0);
            }
            // secilen sarkı baslatılıyor
            else
            {
                play_Selection(selected_songIndex);
            }
        }

        private void button_shuffle_Click(object sender, EventArgs e)
        {
            if (state_buttonShuffle)
            {
                //button_shuffle.Text = "Shuffle OFF";
                button_shuffle.BackColor = Color.Red;
                state_buttonShuffle = false;
            }
            else
            {
                //button_shuffle.Text = "Shuffle ON";
                button_shuffle.BackColor = Color.GreenYellow;
                state_buttonShuffle = true;
            }


        }

        private void button_Loop_Click(object sender, EventArgs e)
        {
            if (state_buttonLoop)
            {
                //button_Loop.Text = "Loop OFF";
                button_Loop.BackColor = Color.Red;
                state_buttonLoop = false;

                button_shuffle.Visible = false;
            }
            else
            {
                //button_Loop.Text = "Loop ON";
                button_Loop.BackColor = Color.GreenYellow;
                state_buttonLoop = true;

                button_shuffle.Visible = true;
            }
        }

        private void listBox_Song_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            play_Selection(selected_songIndex);
        }


        //////////////////////////////
        //////////////////////////////


        public void play_Selection( int listIndex)
        {
            listBox_Song.SelectedIndex = listIndex;

            string song = textBox_URL.Text + "\\" + (string)(listBox_Song.SelectedItem);

            panel_MediaPlayer.URL = song;
            //label_Status.Text = "play selection";
        }

        public void play_Shuffle()
        {            
            Random random = new Random();
            int random_listIndex = random.Next(1, listBox_Song.Items.Count);

            play_Selection(random_listIndex);           
        }

        public void play_Loop()
        {

        }

        private void Mabaduma_FormClosed(object sender, FormClosedEventArgs e)
        {

            Environment.Exit(Environment.ExitCode);

        }

        private void panel_MediaPlayer_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            label_Status.Text = panel_MediaPlayer.status;

            if (e.newState == 8 || e.newState == 1  )//panel_MediaPlayer.playState == WMPLib.WMPPlayState.wmppsMediaEnded || panel_MediaPlayer.status== "Finished" )
            {
                // lopp ve shuffle acıksa surekli karısık calıyor
                if (state_buttonLoop && state_buttonShuffle)
                {
                    play_Shuffle();
                    //label_Status.Text = panel_MediaPlayer.playState.ToString();
                }
                // loop acık, shuffle kapalıysa sarkıları sırayla calıyor
                else if (state_buttonLoop && state_buttonShuffle == false)
                {
                    // son şarkıya gelinmediyse
                    if (selected_songIndex + 1 < listBox_Song.Items.Count)
                    {
                        selected_songIndex += 1;
                        play_Selection(selected_songIndex);
                        // label_Status.Text = panel_MediaPlayer.playState.ToString();
                    }
                    // son şarkıya gelindiyse basa donuluyor
                    else
                    {
                        play_Selection(0);
                        //label_Status.Text = panel_MediaPlayer.playState.ToString();
                    }
                }
            }
            else if (e.newState ==10)
            {
                play_Selection(selected_songIndex);
                panel_MediaPlayer.Ctlcontrols.play();
            }
        }

        private void listBox_Song_SelectedIndexChanged(object sender, EventArgs e)
        {
           selected_songIndex = listBox_Song.SelectedIndex;
        }

    }
}
